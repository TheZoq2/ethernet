use std::{
    io::{Read, Write},
    time::{Duration, Instant},
};

use bitvec::{order::Lsb0, view::BitView};
use camino::Utf8PathBuf;
use clap::Parser;
use color_eyre::{
    eyre::{bail, Context, ContextCompat},
    Result,
};
use mio_serial::{FlowControl, SerialPort};
use pnet::{
    datalink,
    packet::{ethernet::EthernetPacket, Packet},
};

#[derive(Parser)]
struct Args {
    port: Utf8PathBuf,

    /// The eth interface to read from when measuring speed
    interface: Option<String>,
}

fn measure_speed(iface_name: &str, s: &mut dyn SerialPort) -> Result<()> {
    // Change target UART to 1, then send a single byte to trigger a read
    s.write_all(&[0xff, 1, 1, 1])?;
    let interfaces = datalink::interfaces();

    let interface = interfaces
        .into_iter()
        .find(|iface| iface.name == iface_name)
        .context(format!("Failed to find interface {iface_name}"))?;

    let (_, mut rx) = match datalink::channel(&interface, Default::default()) {
        Ok(datalink::Channel::Ethernet(tx, rx)) => (tx, rx),
        Ok(_) => bail!("Unhandled channel type"),
        Err(e) => bail!("Unable to create channel {e}"),
    };

    let num_packets = 1_00_000;
    let mut received_bytes = 0;
    let start = Instant::now();
    for _ in 0..num_packets {
        match rx.next() {
            Ok(packet) => {
                // println!("Got packet");
                let packet = EthernetPacket::new(packet).context("Failed to parse packet")?;

                received_bytes += packet.payload().len();
            }
            Err(e) => bail!("Failed to receive packet: {e}"),
        }
    }
    let end = Instant::now();
    let duration = end - start;

    let seconds = duration.as_secs_f64();
    println!(
        "Received {received_bytes} ({num_packets} packets) in {seconds} ({} MB/s) ({} Mb/s)",
        (received_bytes as f64 / seconds) / 1_000_000.,
        ((received_bytes as f64 * 8.) / seconds) / 1_000_000.
    );

    // Switch back to reading packets
    s.write_all(&[0xff, 1, 0, 0])
        .context("Failed to switch back to reading packets")?;

    Ok(())
}

fn query_mdio(s: &mut dyn SerialPort) -> Result<()> {
    // Change target UART to 0, then send a single byte to trigger a read
    s.write_all(&[0xff, 0, 1, 1])
        .context("Failed to switch to mdmio mode")?;

    let conf = (0..32)
        .map(|_| {
            let mut buf = [0, 0];
            s.read_exact(&mut buf)
                .context("Failed to read 2 bytes from the serial port")?;

            Ok(buf[0] as u16 | ((buf[1] as u16) << 8))
        })
        .collect::<Result<Vec<_>>>()?;

    println!("Raw configruation:");
    for c in &conf {
        println!("{c:04x}")
    }

    println!("Human readable");
    for (i, c) in conf.iter().enumerate() {
        let bits = c.view_bits::<Lsb0>();

        let bool_field = |bit: usize, descr: &str, t: &str, f: &str| {
            println!("  {descr}: {}", if bits[bit] { t } else { f })
        };
        let two_bits =
            |idx: [usize; 2], descr: &str, mapping: &dyn Fn([bool; 2]) -> &'static str| {
                let result = mapping([bits[idx[0]], bits[idx[1]]]);
                println!("  {descr}: {result}")
            };

        println!("{i}");
        match i {
            0 => {
                println!(" Basic Control");
                bool_field(15, "Reset", "reset", "normal");
                bool_field(14, "Loopback", "On", "Off");
                two_bits([6, 13], "Speed select", &|v: [bool; 2]| match v {
                    [true, true] => "reserved",
                    [true, false] => "1000 Mbps",
                    [false, true] => "100 Mbps",
                    [false, false] => "10 Mbps",
                });
                bool_field(12, "Auto negotiation", "enabled", "disabled");
                bool_field(11, "Power-down", "power-down", "normal");
                bool_field(10, "Isolate", "isolated", "normal");
                bool_field(9, "Restart auto negotiation", "on", "normal");
                bool_field(8, "Duplex", "full", "half");
            }
            1 => {
                println!(" Basic status");
                bool_field(15, "100BASE-T4", "capable", "not capable");
                bool_field(14, "100BASE-T4 full-duplex", "capable", "not capable");
                bool_field(13, "100BASE-T4 half-duplex", "capable", "not capable");
                bool_field(12, "10BASE-T4 full-duplex", "capable", "not capable");
                bool_field(11, "10BASE-T4 half-duplex", "capable", "not capable");
                // 10:9 reserved
                bool_field(8, "Extended status", "Yes, in reg15", "no");
                // 7 Reserved
                bool_field(6, "No preamble", "Preamble suppression", "normal");
                bool_field(
                    5,
                    "Automatic negotiation complete",
                    "Complete",
                    "Not complete",
                );
                bool_field(4, "Remote fault", "Yes", "No");
                bool_field(3, "Auto negotiation ability", "Available", "Not Available");
                bool_field(2, "Link status", "Up", "Down");
                bool_field(1, "Jabber", "Detected", "Not detected");
                bool_field(0, "Extended capability", "Supported", "unsupported")
            }
            _ => {}
        }
    }

    // Switch back to reading packets
    s.write_all(&[0xff, 1, 0, 0])
        .context("Failed to switch back to reading packets")?;

    Ok(())
}

fn main() -> Result<()> {
    color_eyre::install()?;
    let args = Args::parse();

    let mut s = mio_serial::new(args.port.as_str(), 115200)
        .flow_control(FlowControl::None)
        .timeout(Duration::from_millis(1_000_000))
        .open()
        .context("Failed to open serial port")?;

    let (tx, rx) = std::sync::mpsc::channel();

    std::thread::spawn(move || {
        let stdin = std::io::stdin();
        loop {
            let mut line = String::new();
            stdin.read_line(&mut line).unwrap();

            tx.send(line).unwrap();
        }
    });

    // Configure to start reading received packets
    s.write_all(&[0xff, 1, 0, 0])
        .context("Failed to start to reading packets")?;

    println!(">> ");

    loop {
        if s.bytes_to_read()? != 0 {
            let mut buf = [0; 32];
            let count = s.read(&mut buf)?;
            for b in &buf[0..count] {
                print!("{b:02x} ")
            }
            println!("")
        }

        match rx.try_recv() {
            Ok(val) => {
                println!("Command: {val}");
                if val.trim() == "mdio" {
                    println!("Entering MDIO mode");
                    query_mdio(s.as_mut()).context("Failed to read mdio")?;
                } else if val.trim() == "bench" {
                    println!("Starting benchmark");
                    if let Some(interface) = &args.interface {
                        measure_speed(&interface, s.as_mut())?;
                    } else {
                        println!("--interface must be specified")
                    }
                }
                println!(">> ")
            }
            _ => {}
        }
    }
}
