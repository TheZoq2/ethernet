// (c)EMARD
// License=BSD

// parametric ECP5 PLL generator in systemverilog
// actual frequency can be equal or higher than requested
// to see actual frequencies
// trellis log/stdout : search for "MHz", "Derived", "frequency"
// to see actual phase shifts
// diamond log/*.mrp  : search for "Phase", "Desired"

`default_nettype none

module clk125gen(
  input clk30,
  output output__
);
  wire[3:0] out;
  ecp5pll#(.in_hz(30000000), .out0_hz(125000000)) pll (.clk_i(clk30), .clk_o(out));
  assign output__ = out[0];
endmodule
