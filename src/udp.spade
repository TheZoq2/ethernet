use std::ports::new_mut_wire;
use std::ports::read_mut_wire;
use std::conv::concat_arrays;

use ready_valid::lib::Rv;

use lib::header_payload_stream::Header;
use lib::header_payload_stream::HeaderPayloadStreamO;
use lib::header_payload_stream::HeaderLowerer;
use lib::ip::IpStream;
use lib::ip::IpAddr;
use lib::ip::IpHeader;

struct UdpHeader {
    source_port: Option<uint<16>>,
    destination_port: uint<16>,
    /// UDP specifies this to support packets around d65535 bytes
    /// wide, but currently we only allow packets that fit in single UDP
    /// frames, i.e. around 1450 bytes
    payload_length: uint<16>,
}

// The actual impl should be
/*
impl Serialize for UdpHeader {
    type Size = 8;
    ...
}
*/
impl UdpHeader {
    fn serialize(self) -> [uint<8>; 8] {
        let source_port = match self.source_port {
            Some(p) => p,
            None => 0
        };
        source_port.to_be_bytes() `concat_arrays`
            self.destination_port.to_be_bytes() `concat_arrays`
            self.total_length().to_be_bytes() `concat_arrays`
            [0, 0]
    }
}

impl UdpHeader {
    /// Total length of the packet including 8 bytes of header.
    fn total_length(self) -> uint<16> {
        trunc(self.payload_length + 8)
    }

}

struct port UdpStream {
    inner: HeaderPayloadStreamO<UdpHeader>
}

struct IpStreamLowerer {
    source_ip: IpAddr,
    dest_ip: IpAddr,
}
impl HeaderLowerer<IpHeader> for IpStreamLowerer {
    fn generate(self, payload_length: uint<16>) -> IpHeader {
        IpHeader$(
            dest: self.dest_ip,
            source: self.source_ip,
            dscp: 0,
            flags: 0,
            fragment_offset: 0,
            identification: 0,
            protocol: 0x11,
            payload_length: zext(payload_length),
            ecn: 0,
            ttl: 64,
        )
    }
}

impl UdpStream {
    entity into_ip(
        self,
        clk: clock,
        rst: bool,
        source_ip: IpAddr,
        dest_ip: IpAddr,
    ) -> IpStream {
        IpStream(self
            .inner
            .inst lower(clk, rst, IpStreamLowerer$(source_ip, dest_ip)))
    }

}

mod test {
    use std::ports::new_mut_wire;
    use std::ports::read_mut_wire;

    use ready_valid::lib::Rv;

    use lib::ip::IpAddr;
    use lib::ip::IpHeader;
    use lib::ip::IpStream;

    use lib::udp::UdpHeader;
    use lib::udp::UdpStream;
    use lib::header_payload_stream::HeaderPayloadStreamO;

    struct ThOut {
        ip_headers: Option<IpHeader>,
        ip_payload: Option<uint<8>>,
        payload_ready: bool,
        udp_headers_ready: bool,
    }

    /*
               UDP Headers  UDP Ready
                   |            ^
                   V            |
                +------------------+
                |       DUT        |
                +------------------+
           |                          ^
           v                          |
        IP Headers,payloa  IP Headers, payload ready    
    */

    entity into_ip_th(
        clk: clock,
        rst: bool,
        udp_headers: Option<UdpHeader>,
        udp_payload: Option<uint<8>>,
        source_ip: IpAddr,
        dest_ip: IpAddr,
        ip_headers_ready: bool,
        ip_payload_ready: bool,
    ) -> ThOut {
        let headers_ready = inst new_mut_wire();
        let payload_ready = inst new_mut_wire();
        let s = UdpStream(
            HeaderPayloadStreamO(
                Rv(&udp_headers, headers_ready),
                Rv(&udp_payload, payload_ready),
            )
        );

        let out = s.inst into_ip$(clk, rst, source_ip, dest_ip);
        let IpStream(HeaderPayloadStreamO$(headers: ip_headers, bytes: ip_payload)) = out;
        set ip_payload.ready = ip_payload_ready;
        set ip_headers.ready = ip_headers_ready;

        ThOut$(
            ip_headers: *ip_headers.data,
            ip_payload: *ip_payload.data,
            payload_ready: inst read_mut_wire(payload_ready),
            udp_headers_ready: inst read_mut_wire(headers_ready),
        )
    }
}

