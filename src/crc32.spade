use std::conv::bits_to_uint;

// TODO: Hoist into stdlib
impl uint<32> {
    fn bitreverse(self) -> uint<32> {
        std::conv::bits_to_uint(std::conv::flip_array(self.bits()))
    }
}

struct CrcVal {
    inner: uint<32>
}

// e: 01011010111110100010000011100100
// g: 00100111000001000101111101011010

impl CrcVal {
    fn crc32_d1(self, d: bool) -> CrcVal {
        let c = self.inner.bits();
        let result = [
            c[31] ^^ d,                     // x^0
            c[0] ^^ d ^^ c[31],             // x^1
            c[1] ^^ d ^^ c[31],             // x^2
            c[2],                           // x^3
            c[3] ^^ d ^^ c[31],             // x^4
            c[4] ^^ d ^^ c[31],             // x^5
            c[5],                           // x^6
            c[6] ^^ d ^^ c[31],             // x^7
            c[7] ^^ d ^^ c[31],             // x^8
            c[8],                           // x^9
            c[9] ^^ d ^^ c[31],             // x^10
            c[10] ^^ d ^^ c[31],            // x^11
            c[11] ^^ d ^^ c[31],            // x^12
            c[12],                          // x^13
            c[13],                          // x^14
            c[14],                          // x^15
            c[15] ^^ d ^^ c[31],            // x^16
            c[16],                          // x^17
            c[17],                          // x^18
            c[18],                          // x^19
            c[19],                          // x^20
            c[20],                          // x^21
            c[21] ^^ d ^^ c[31],            // x^22
            c[22] ^^ d ^^ c[31],            // x^23
            c[23],                          // x^24
            c[24],                          // x^25
            c[25] ^^ d ^^ c[31],            // x^26
            c[26],                          // x^27
            c[27],                          // x^28
            c[28],                          // x^29
            c[29],                          // x^30
            c[30],                          // x^31
        ];
        CrcVal(bits_to_uint(result))
    }
}

fn crc32_d8(d: uint<8>, c: uint<32>) -> uint<32> {
    let d = d.bits();
    CrcVal(c)
        .crc32_d1(d[0])
        .crc32_d1(d[1])
        .crc32_d1(d[2])
        .crc32_d1(d[3])
        .crc32_d1(d[4])
        .crc32_d1(d[5])
        .crc32_d1(d[6])
        .crc32_d1(d[7])
        .inner
}
