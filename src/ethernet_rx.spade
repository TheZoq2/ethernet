use lib::ethernet::EthernetHeader;
use lib::ethernet::MacAddr;
use lib::rgmii_rx::RgmiiRx;
use lib::arp::ArpPacket;
use lib::arp::ArpStream;
use std::conv::unsafe::unsafe_cast;

enum Diagnostic {
    /// The interpacket gap appeared in the middle of a header
    IPGInHeader,
    /// The inter packet gap was shorter than expected
    ShortIPG
}

struct EthRx {
    headers: Option<EthernetHeader>,
    payload: Option<uint<8>>,
    diagnostics: Option<Diagnostic>,
}


impl EthRx {
    entity filter_packets<F>(self, clk: clock, rst: bool, f: F) -> EthRx
        where F: PacketFilter
    {
        let accept_this_header = match self.headers {
            Some(h) => f.accept_header(h),
            None => false
        };

        reg(clk) found_header reset(rst: false) =
            match (found_header, accept_this_header, self.payload) {
                // If we get a new header, regardless of current state, we accept the
                // packet if it matches the header
                (_, true, _) => true,
                // If we have already found the header and get payload, we continue accepting
                // payload
                (true, _, Some(payload)) => true,
                // Otherwise, we unaccept
                (_, _, _) => false
            };

        EthRx $(
            headers: if accept_this_header {self.headers} else {None},
            payload: if found_header {self.payload} else {None},
            diagnostics: self.diagnostics,
        )
    }

    entity filter_arp_packets(self, clk: clock, rst: bool, self_mac: MacAddr) -> EthRx {
        let broadcast_mac = MacAddr([0xff, 0xff, 0xff, 0xff, 0xff, 0xff]);

        let f = (DestFilter(self_mac) `OrFilter` DestFilter(broadcast_mac))
            `AndFilter` TypeFilter(0x0806);

        self.inst filter_packets(clk, rst, f)
    }

}

struct InnerPayload<#uint HeaderSize> {
    headers: Option<[uint<8>; HeaderSize]>,
    payload: Option<uint<8>>,
}

enum IntoFixedSizeHeaderState<#uint N, #uint M> {
    Idle,
    ReceiveHeader{left: uint<M>, bytes: [uint<8>; N]},
    ReceivePayload
}

impl EthRx {
    entity into_fixed_size_inner_header<#uint N>(self, clk: clock, rst: bool) -> InnerPayload<N> {
        reg(clk) state
            : IntoFixedSizeHeaderState<N, {uint_bits_to_fit(N)}>
            reset(rst: IntoFixedSizeHeaderState::Idle) =
            match state{
                IntoFixedSizeHeaderState::Idle => {
                    match self.headers {
                        Some(header) => {
                            IntoFixedSizeHeaderState::ReceiveHeader(trunc(N-1u32), [0; N])
                        },
                        None => IntoFixedSizeHeaderState::Idle,
                    }
                },
                IntoFixedSizeHeaderState::ReceiveHeader$(left: 0, bytes) => {
                    if self.payload.is_none() {
                        // TODO: We should probably emit an error here
                        IntoFixedSizeHeaderState::Idle
                    } else {
                        IntoFixedSizeHeaderState::ReceivePayload
                    }
                },
                IntoFixedSizeHeaderState::ReceiveHeader$(left, bytes) => {
                    match self.payload {
                        Some(new_byte) => {
                            IntoFixedSizeHeaderState::ReceiveHeader$(
                                left: trunc(left - 1),
                                bytes: bytes[1:N] `concat_arrays` [new_byte]
                            )
                        },
                        None =>  IntoFixedSizeHeaderState::Idle
                    }
                },
                IntoFixedSizeHeaderState::ReceivePayload => {
                    if self.payload.is_some() {
                        IntoFixedSizeHeaderState::ReceivePayload
                    } else {
                        IntoFixedSizeHeaderState::Idle
                    }
                }
            };

        let headers = match state {
            IntoFixedSizeHeaderState::Idle => None,
            IntoFixedSizeHeaderState::ReceiveHeader$(left: 0, bytes) => {
                match self.payload {
                    Some(new_byte) => {
                        Some(bytes[1:N] `concat_arrays` [new_byte])
                    },
                    None => None
                }
            },
            IntoFixedSizeHeaderState::ReceiveHeader(_, _) => None,
            IntoFixedSizeHeaderState::ReceivePayload => None,
        };

        let payload = match state {
            IntoFixedSizeHeaderState::Idle => None,
            IntoFixedSizeHeaderState::ReceiveHeader(_, _) => None,
            IntoFixedSizeHeaderState::ReceivePayload => self.payload,
        };

        InnerPayload$(headers, payload)
    }

    entity into_arp(self, clk: clock, rst: bool) -> ArpStream {
        let inner = self.inst into_fixed_size_inner_header(clk, rst);

        ArpStream(match inner.headers {
            Some(bytes) => Some(lib::arp::deserialize_arp(bytes)),
            None => None,
        })
    }
}


/////////////////////////////////////////////////////////////////////////////////////
// Packet filters
/////////////////////////////////////////////////////////////////////////////////////

trait PacketFilter {
    fn accept_header(self, header: EthernetHeader) -> bool;
}

struct SourceFilter {
    expected_mac: MacAddr
}

impl PacketFilter for SourceFilter {
    fn accept_header(self, header: EthernetHeader) -> bool {
        // We only support == on Number<T>, but we can cheat
        unsafe_cast::<_, uint<48>>(header.source) == unsafe_cast(self.expected_mac)
    }
}

struct DestFilter {
    expected_mac: MacAddr
}

impl PacketFilter for DestFilter {
    fn accept_header(self, header: EthernetHeader) -> bool {
        // We only support == on Number<T>, but we can cheat
        unsafe_cast::<_, uint<48>>(header.dest) == unsafe_cast(self.expected_mac)
    }
}

struct TypeFilter {
    expected_type: uint<16>
}

impl PacketFilter for TypeFilter {
    fn accept_header(self, header: EthernetHeader) -> bool {
        header.type_or_len == self.expected_type
    }
}


struct OrFilter<L, R> {
    l: L,
    r: R
}

impl<L, R> PacketFilter for OrFilter<L, R>
    where L: PacketFilter,
          R: PacketFilter,
{
    fn accept_header(self, header: EthernetHeader) -> bool {
        self.l.accept_header(header) || self.r.accept_header(header)
    }
}

struct AndFilter<L, R> {
    l: L,
    r: R,
}

impl<L, R> PacketFilter for AndFilter<L, R>
    where L: PacketFilter,
          R: PacketFilter,
{
    fn accept_header(self, header: EthernetHeader) -> bool {
        self.l.accept_header(header) && self.r.accept_header(header)
    }
}

/////////////////////////////////////////////////////////////////////////////////////
// Test harnesses
/////////////////////////////////////////////////////////////////////////////////////

entity packet_filter_th(self_: EthRx, clk: clock, rst: bool, source_addr: MacAddr) -> EthRx {
    self_.inst filter_packets(clk, rst, SourceFilter(source_addr))
}

entity arp_filter_th(self_: EthRx, clk: clock, rst: bool, self_mac: MacAddr) -> EthRx {
    self_.inst filter_arp_packets$(clk, rst, self_mac)
}

entity into_fixed_size_inner_header_th(self_: EthRx, clk: clock, rst: bool) -> InnerPayload<6> {
    self_.inst into_fixed_size_inner_header(clk, rst)
}
