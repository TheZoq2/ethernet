# top = crc32::crc32_d8

from typing import Tuple
from cocotb.clock import Clock
from spade import SpadeExt
from cocotb import cocotb
from cocotb.triggers import Timer, FallingEdge




@cocotb.test()
async def one_byte_works(dut):
    s = SpadeExt(dut) # Wrap the dut in the Spade wrapper

    s.i.c = 0xffff_ffff
    s.i.d = 0x01
    await Timer(1, units="ns")
    s.o.assert_eq(f"0x27045f5a")


