# top = udp::test::into_ip_th

from cocotb.clock import Clock, Timer
from spade import SpadeExt
from cocotb import cocotb
from cocotb.triggers import FallingEdge

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut) # Wrap the dut in the Spade wrapper

    clk = dut.clk_i

    async def payload_driver():
        data = list(map(lambda x: 0x10+x, range(0,100)))
        idx = 0
        while True:
            if idx >= len(data):
                s.i.payload = "None"
                break
            s.i.payload = f"Some({data[idx]})"
            if s.o.payload_ready == True:
                idx += 1
            await FallingEdge(clk)

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())

    payload_length = 42
    total_length = payload_length + 8
    udp_header = f"""
        Some(UdpHeader$(
            source_port: Some(0xabcd),
            destination_port: 0x1234,
            payload_length: {payload_length}
        ))
    """
    ip_header = f"""
        Some(IpHeader$(
            dest: ethernet::ip::IpAddr([5,6,7,8]),
            source: ethernet::ip::IpAddr([1,2,3,4]),
            dscp: 0,
            flags: 0,
            fragment_offset: 0,
            identification: 0,
            protocol: 0x11,
            payload_length: {total_length},
            ecn: 0,
            ttl: 64,
        ))
    """
    expected_ip_header = [
        0xab, 0xcd,
        0x12, 0x34,
        ((total_length >> 8) & 0xff),
        total_length & 0xff, 0, 0
    ]
    expected_payload = list(map(lambda x: 0x10 + x, range(0, payload_length)))

    s.i.udp_headers = "None"
    s.i.source_ip = "ethernet::ip::IpAddr([1,2,3,4])"
    s.i.dest_ip = "ethernet::ip::IpAddr([5,6,7,8])"
    s.i.udp_payload = "None"
    s.i.rst = True
    [await FallingEdge(clk) for i in range(0, 10)]
    s.i.rst = False
    s.i.ip_headers_ready = False

    # While we have not given any UDP packets to process, we won't be reading payload
    # And we are ready to receive new headers
    s.o.payload_ready.assert_eq(False)
    s.o.udp_headers_ready.assert_eq(True)

    await FallingEdge(clk);
    s.i.udp_headers = udp_header
    await FallingEdge(clk)
    s.i.udp_headers = "None"

    # After providing a udp, we we start producing an IP header.
    s.o.ip_headers.assert_eq(ip_header)
    # However, if we deassert ip_headers_ready, we we don't consume that header
    s.i.ip_headers_ready = False
    # But we cannot accept any more UDP headers
    s.o.udp_headers_ready.assert_eq(False)
    await FallingEdge(clk)
    # Since we de-asserted the ip_headers_ready we're still seeing an IP header
    s.o.ip_headers.assert_eq(ip_header)
    s.o.udp_headers_ready.assert_eq(False)

    # Consume the header
    s.i.ip_headers_ready = True
    # Don't consume the subsequent payload for a while
    s.i.ip_payload_ready = False
    await FallingEdge(clk)

    # After consuming the IP header, we no longer produce IP headers, we're not ready
    # to receive more UDP headers. We now output the first byte of the UDP header but don't
    # move out of that state until we've consumed payload

    for i in range(0, 5):
        s.o.ip_payload.assert_eq(f"Some({expected_ip_header[0]})")
        await FallingEdge(clk)
    s.i.ip_payload_ready = True

    # We now expect the header
    for b in expected_ip_header:
        s.o.ip_payload.assert_eq(f"Some({b})")
        # But we're not going to consume from upstream 
        s.o.payload_ready.assert_eq(False)
        await FallingEdge(clk)

    # After the header, we expect the UDP payload
    for b in expected_payload:
        s.i.udp_payload = f"Some({b})"
        await Timer(1, units = "ns")
        s.o.ip_payload.assert_eq(f"Some({b})")
        s.o.payload_ready.assert_eq(True)
        await FallingEdge(clk)

    s.o.payload_ready.assert_eq(False)
    s.o.udp_headers_ready.assert_eq(True)

