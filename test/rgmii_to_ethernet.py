# top = rgmii_rx::into_ethernet_th

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import FallingEdge, Timer
from spade import SpadeExt

async def well_formed_header(clk, s: SpadeExt, i):
    async def set_1cc(value):
        s.i.self_ = value
        await FallingEdge(clk)

    dest_mac = [10 + i, 20+i, 30+i, 40+8, 50+8, 60+8]
    source_mac = [10+i+5, 20+i+5, 30+i+5, 40+i, 50+i, 60+i]
    for _ in range(0, 7):
        await set_1cc("Some(0x55)")
    await set_1cc("Some(0xD5)")

    for v in dest_mac:
        await set_1cc(f"Some({v})")

    for v in source_mac:
        await set_1cc(f"Some({v})")
    
    eth_type = [0, i*2]

    s.i.self_ = f"Some({eth_type[1]})"
    await FallingEdge(clk)
    s.i.self_ = f"Some({eth_type[0]})"
    await Timer(1, units="ps")

    # Expect a header and payload
    s.o.headers.assert_eq(f"""
        Some(ethernet::ethernet::EthernetHeader$(
            dest: ethernet::ethernet::MacAddr({dest_mac}),
            source: ethernet::ethernet::MacAddr({source_mac}),
            type_or_len: {i*2}
        ))
    """)

async def well_formed_payload(clk, s, i):
    for i in range(20, 30):
        s.i.self_ = f"Some({i})";
        await FallingEdge(clk)
        s.o.payload.assert_eq(f"Some({i})")
        s.o.headers.assert_eq("None")

async def well_formed_frame(clk, s: SpadeExt, i):
    await well_formed_header(clk, s, i)
    await well_formed_payload(clk, s, i)


@cocotb.test()
async def well_formed_frames(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units="ns").start())

    print("Started up")

    s.i.self_ = "None"

    s.i.rst = True
    await FallingEdge(clk)
    s.i.rst = False

    for i in range(1, 4):
        await well_formed_frame(clk, s, i)

        # IPG
        for i in range(0, 12):
            s.i.self_ = "None"
            await FallingEdge(clk)
            s.o.payload.assert_eq("None")
            s.o.headers.assert_eq("None")



@cocotb.test()
async def short_ipg(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units="ns").start())

    print("Started up")

    s.i.self_ = "None"

    s.i.rst = True
    await FallingEdge(clk)
    s.i.rst = False

    async def set_1cc(value):
        s.i.self_ = value
        await FallingEdge(clk)
        

    for i in range(1, 4):
        if i != 1:
            async def check_error():
                await Timer(1, units="ps");
                s.o.diagnostics.assert_eq("Some(Diagnostic::ShortIPG)")

            await cocotb.start(check_error())

        await well_formed_frame(clk, s, i)

        # IPG
        for i in range(0, 1):
            s.i.self_ = "None"
            await FallingEdge(clk)
            s.o.payload.assert_eq("None")
            s.o.headers.assert_eq("None")


@cocotb.test()
async def ipg_in_header(dut):
    
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units="ns").start())

    print("Started up")

    s.i.self_ = "None"

    s.i.rst = True
    await FallingEdge(clk)
    s.i.rst = False


    for i in range(1, 4):
        # We won't emit an IPG error if we're in the prelude, but ocne we get into the header
        # we will
        preamble_len = 8
        header_size = 14
        for byte in range(preamble_len, preamble_len + header_size + 1):
            header_task = await cocotb.start(well_formed_header(clk, s, i))

            for _ in range(0, byte):
                await FallingEdge(clk)

            # We only want to see a diagnostic if we're inside the header
            if byte < preamble_len + header_size:
                header_task.cancel()
                s.i.self_ = "None"
                await Timer(1, units="ps")
                s.o.diagnostics.assert_eq("Some(Diagnostic::IPGInHeader)")
            else:
                payload_task = await cocotb.start(well_formed_payload(clk, s, i))
                async def ensure_none():
                    s.o.diagnostics.assert_eq("None")
                    await FallingEdge(clk)
                await cocotb.start_soon(ensure_none())
                await payload_task.join()


            # IPG
            for i in range(0, 12):
                s.i.self_ = "None"
                await FallingEdge(clk)
                s.o.payload.assert_eq("None")
                s.o.headers.assert_eq("None")

