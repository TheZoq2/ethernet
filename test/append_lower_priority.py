# top = header_payload_stream::append_lower_priority_th

from typing import Tuple
from cocotb.clock import Clock
from spade import SpadeExt
from cocotb import cocotb
from cocotb.triggers import Timer, FallingEdge




@cocotb.test()
async def header_selection(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())

    s.i.self_.headers.data = "&None"
    s.i.other.headers.data = "&None"
    s.i.self_.bytes.data = "&Some(1)"
    s.i.other.bytes.data = "&Some(2)"

    s.o.headers.ready = "&false"
    s.o.bytes.ready = "&false"

    await FallingEdge(clk)
    s.i.rst = True
    await FallingEdge(clk)

    # If neither side wants to transmit, there should be no transmission
    s.o.headers.data.assert_eq("&None")

    await FallingEdge(clk)

    # If both want to transmit, self has priority
    s.i.self_.headers.data = "&Some((1, 1))"
    s.i.other.headers.data = "&Some((2, 2))"

    await FallingEdge(clk)

    s.o.headers.data.assert_eq("&Some((1, 1))")

    s.i.self_.headers.data = "&Some((1, 1))"
    s.i.other.headers.data = "&None"

    await FallingEdge(clk)

    s.o.headers.data.assert_eq("&Some((1, 1))")

    await FallingEdge(clk)

    # Other transmits if self is unset
    s.i.self_.headers.data = "&None"
    s.i.other.headers.data = "&Some((2, 2))"

    await FallingEdge(clk)

    s.o.headers.data.assert_eq("&Some((2, 2))")

    

@cocotb.test()
async def payload_stickiness(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())

    s.i.self_.headers.data = "&None"
    s.i.other.headers.data = "&None"
    s.i.self_.bytes.data = "&Some(1)"
    s.i.other.bytes.data = "&Some(2)"

    s.o.headers.ready = "&false"
    s.o.bytes.ready = "&false"

    await FallingEdge(clk)
    s.i.rst = True
    await FallingEdge(clk)
    s.i.rst = False

    # Both transmit, and downstream is ready for one cycle. This latches the payload to
    # self_
    s.i.self_.headers.data = "&Some((1, 1))"
    s.i.other.headers.data = "&Some((2, 2))"
    s.o.headers.ready = "&true"
    s.o.bytes.ready = "&true"

    await FallingEdge(clk)
    s.o.headers.ready = "&false"

    s.i.self_.bytes.ready.assert_eq("&true")
    s.i.other.bytes.ready.assert_eq("&false")
    s.o.bytes.data.assert_eq("&Some(1)");

    s.i.self_.headers.data = "&None"
    s.i.other.headers.data = "&Some((2, 2))"

    await FallingEdge(clk)
    s.i.self_.bytes.ready.assert_eq("&true")
    s.i.other.bytes.ready.assert_eq("&false")
    s.o.bytes.data.assert_eq("&Some(1)");

    s.i.self_.headers.data = "&None"
    s.i.other.headers.data = "&None"

    await FallingEdge(clk)
    s.i.self_.bytes.ready.assert_eq("&true")
    s.i.other.bytes.ready.assert_eq("&false")
    s.o.bytes.data.assert_eq("&Some(1)");

    # If upstream accepts another header, the payload is switched
    s.i.self_.headers.data = "&None"
    s.i.other.headers.data = "&Some((2, 2))"
    s.o.headers.ready = "&true"

    await FallingEdge(clk)
    s.o.headers.ready = "&false"

    s.o.bytes.data.assert_eq("&Some(2)")
    s.i.self_.bytes.ready.assert_eq("&false")
    s.i.other.bytes.ready.assert_eq("&true")
