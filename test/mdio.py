# top = mdio::mdio_test_harness

from typing import Tuple
from cocotb.clock import Clock
from spade import SpadeExt
from cocotb import cocotb
from cocotb.triggers import FallingEdge

async def respond_read(
    clk,
    s: SpadeExt,
    expected_device: int,
    expected_address: int,
    data: int
):
    async def rising_mdc():
        # Wait for a currently rising edge to fall
        while s.o.mdc == True:
            await FallingEdge(clk)
        while s.o.mdc == False:
            await FallingEdge(clk)

    async def read_wide(bits: int) -> int:
        read = 0
        for i in range(0, bits):
            await rising_mdc()
            if s.o.drive_low == False:
                read = (read << 1) | 1
            else:
                read = (read << 1)
        return read

    async def expect_wide(bits: int, expected: int):
        assert expected == await read_wide(bits)


    # 32 bits of preamble
    for _ in range(0, 32):
        await rising_mdc()
        s.o.drive_low.assert_eq(False)

    # St
    await expect_wide(2, 0b01)

    # Op
    await expect_wide(2, 0b10)

    # Device and address
    await expect_wide(5, expected_device)
    await expect_wide(5, expected_address)

    #TA
    await expect_wide(2, 0b11)

    for _ in range(0, 16):
        s.i.mdio_in = (data & 0x8000) != 0
        data = (data << 1) & 0xffff
        await rising_mdc()
        print(f"{data:b}: {(data & 0x8000) != 0}")



async def initial_setup(dut) -> Tuple[object, SpadeExt]:
    s = SpadeExt(dut) # Wrap the dut in the Spade wrapper

    # To access unmangled signals as cocotb values (without the spade wrapping) use
    # <signal_name>_i
    # For cocotb functions like the clock generator, we need a cocotb value
    clk = dut.clk_i

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())


    s.i.command = "None"
    s.i.bit_duration = 10
    s.i.mdio_in = False
    s.i.rst = True
    await FallingEdge(clk);
    s.i.rst = False

    s.o.read_value.assert_eq("None")
    s.o.mdc.assert_eq(False)
    s.o.drive_low.assert_eq(False)
    await FallingEdge(clk);

    return (clk, s)

@cocotb.test()
async def test(dut):
    (clk, s) = await initial_setup(dut)

    s.i.command = "Some(MdioCommand::Read$(device: 0b10101, addr: 0b11001))"
    await FallingEdge(clk)
    s.i.command = "None"

    expected = 0x77bb
    await respond_read(clk, s, 0b10101, 0b11001, expected)

    # Within one bit duration we expect the reading to be done
    for _ in range(0, 10):
        if s.o.read_value != "None":
            s.o.read_value.assert_eq(f"Some({expected})")
        await FallingEdge(clk)


@cocotb.test()
async def leading_1_is_read(dut):
    (clk, s) = await initial_setup(dut)

    s.i.command = "Some(MdioCommand::Read$(device: 0b10101, addr: 0b11001))"
    await FallingEdge(clk)
    s.i.command = "None"

    expected = 0x8000
    await respond_read(clk, s, 0b10101, 0b11001, expected)

    # Within one bit duration we expect the reading to be done
    for _ in range(0, 10):
        if s.o.read_value != "None":
            s.o.read_value.assert_eq(f"Some({expected})")
        await FallingEdge(clk)
