#top = ethernet_rx::into_fixed_size_inner_header_th

import cocotb
from spade import SpadeExt
from cocotb.triggers import FallingEdge, Timer
from cocotb.clock import Clock

@cocotb.test()
async def it_works(dut):
    s = SpadeExt(dut)
    clk = dut.clk_i

    await cocotb.start(Clock(clk, 1, units="ns").start())

    await FallingEdge(clk)
    s.i.rst = "true"
    await FallingEdge(clk)
    s.i.rst = "false"

    s.i.self_.headers = "None"
    s.i.self_.payload = "None"

    for i in range(0, 10):
        s.o.headers.assert_eq("None")
        s.o.payload.assert_eq("None")

    s.i.self_.headers = "Some(EthernetHeader(MacAddr([0,0,0,0,0,0]), MacAddr([1,2,3,4,5,6]), 10))"
    await FallingEdge(clk)
    for i in range(0, 5):
        s.i.self_.payload = f"Some({i + 10})"
        await FallingEdge(clk)
    s.i.self_.payload = f"Some(20)"
    await Timer(1, units="ps")
    s.o.headers.assert_eq("Some([10, 11, 12, 13, 14, 20])")
    s.i.self_.payload = f"Some(30)"
    await FallingEdge(clk)
    s.o.payload.assert_eq("Some(30)")
