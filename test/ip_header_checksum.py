#top = ip::ip_header_checksum

from cocotb.clock import Timer
from spade import SpadeExt
from cocotb import cocotb

@cocotb.test()
async def wikipedia_test(dut):
    s = SpadeExt(dut)

    s.i.words = [
        0x4500, 0x0073, 0x0000, 0x4000, 0x4011, 0xc0a8, 0x0001,
        0xc0a8, 0x00c7
    ];
    await Timer(1, units="ns")
    s.o.assert_eq(0xb861)
