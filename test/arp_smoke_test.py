# top=arp::arp_smoke_test

import cocotb
from cocotb.triggers import FallingEdge, Timer
from cocotb.clock import Clock
from spade import SpadeExt

@cocotb.test()
async def tests(dut):
    s = SpadeExt(dut)
    clk = dut.clk_i

    await cocotb.start(Clock(clk, 1, units="ns").start())

    found_packet = False
    async def watcher(expected):
        nonlocal found_packet
        while True:
            await FallingEdge(clk)
            await Timer(100, units="ps")
            if s.o != "ArpStream(None)":
                s.o.assert_eq(expected)
                found_packet = True
                break


    await FallingEdge(clk)
    s.i.rst = True
    s.i.bytes = "None"
    s.i.self_mac = [0x90,0x91,0x92,0x93,0x94,0x95]
    await FallingEdge(clk)
    s.i.rst = False

    await cocotb.start(watcher(
       """
           ArpStream(Some(ArpPacket$(
               hardware_type: 1,
               protocol_type: 0x0800,
               hardware_length: 6,
               protocol_length: 4,
               operation: 1,
               sender_hardware_address: MacAddr([0x10, 0x11, 0x12, 0x13, 0x14, 0x15]),
               sender_protocol_address: IpAddr([0x20, 0x21, 0x22, 0x23]),
               target_hardware_address: MacAddr([0x30, 0x31, 0x32, 0x33, 0x34, 0x35]),
               target_protocol_address: IpAddr([0x40, 0x41, 0x42, 0x43])
           )))
       """
   ));


    async def send_byte(byte):
        s.i.bytes = f"Some({byte})"
        await FallingEdge(clk)


    async def send_mac(mac):
        for i in range(0, 6):
            await send_byte(mac[i])


    async def send_u16(val):
        await send_byte((val >> 8) & 0xff)
        await send_byte(val & 0xff)

    [await send_byte(0x55) for i in range(0, 7)]
    await send_byte(0xD5)

    await send_mac([0xff, 0xff, 0xff, 0xff, 0xff, 0xff])
    await send_mac([1,2,3,4,5,6])
    # Ethertype
    await send_u16(0x0806)

    # ARP packet
    ## Hardware type
    await send_u16(1)
    ## Protocol type
    await send_u16(0x0800)
    ## Hardware length
    await send_byte(6)
    ## Protocol length
    await send_byte(4)
    ## Operation
    await send_u16(1)
    ## Sender hardware address
    [await send_byte(b) for b in [0x10, 0x11, 0x12, 0x13, 0x14, 0x15]]
    ## Sender protocol address
    [await send_byte(b) for b in [0x20, 0x21, 0x22, 0x23]]
    ## Target hardware address
    [await send_byte(b) for b in [0x30, 0x31, 0x32, 0x33, 0x34, 0x35]]
    ## Target protocol address
    [await send_byte(b) for b in [0x40, 0x41, 0x42, 0x43]]

    for _ in range(0, 12):
        s.i.bytes = "None"
        await FallingEdge(clk)

    assert found_packet
    found_packet = False

    # Now looking for our own address

    await cocotb.start(watcher(
       """
           ArpStream(Some(ArpPacket$(
               hardware_type: 1,
               protocol_type: 0x0800,
               hardware_length: 6,
               protocol_length: 4,
               operation: 1,
               sender_hardware_address: MacAddr([0x10, 0x11, 0x12, 0x13, 0x14, 0x15]),
               sender_protocol_address: IpAddr([0x20, 0x21, 0x22, 0x23]),
               target_hardware_address: MacAddr([0x30, 0x31, 0x32, 0x33, 0x34, 0x35]),
               target_protocol_address: IpAddr([0x40, 0x41, 0x42, 0x43])
           )))
       """
   ));

    [await send_byte(0x55) for _ in range(0, 7)]
    await send_byte(0xD5)

    await send_mac([0x90, 0x91, 0x92, 0x93, 0x94, 0x95])
    await send_mac([1,2,3,4,5,6])
    # Ethertype
    await send_u16(0x0806)

    # ARP packet
    ## Hardware type
    await send_u16(1)
    ## Protocol type
    await send_u16(0x0800)
    ## Hardware length
    await send_byte(6)
    ## Protocol length
    await send_byte(4)
    ## Operation
    await send_u16(1)
    ## Sender hardware address
    [await send_byte(b) for b in [0x10, 0x11, 0x12, 0x13, 0x14, 0x15]]
    ## Sender protocol address
    [await send_byte(b) for b in [0x20, 0x21, 0x22, 0x23]]
    ## Target hardware address
    [await send_byte(b) for b in [0x30, 0x31, 0x32, 0x33, 0x34, 0x35]]
    ## Target protocol address
    [await send_byte(b) for b in [0x40, 0x41, 0x42, 0x43]]

    for _ in range(0, 12):
        s.i.bytes = "None"
        await FallingEdge(clk)

    assert found_packet

    
