# top = ethernet_rx::packet_filter_th

from spade import SpadeExt
import cocotb
from cocotb.triggers import FallingEdge, Timer
from cocotb.clock import Clock

@cocotb.test()
async def packet_filter_test(dut):
    s = SpadeExt(dut)
    clk = dut.clk_i

    await cocotb.start(Clock(clk, period=10, units="ns").start())

    await FallingEdge(clk)
    s.i.rst = True;
    s.i.source_addr = "MacAddr([1,2,3,4,5,6])"
    s.i.self_.headers = "None"
    s.i.self_.payload = "None"
    await FallingEdge(clk)
    s.i.rst = False

    s.o.headers.assert_eq("None")
    s.o.payload.assert_eq("None")

    await FallingEdge(clk)

    accepted_header =  """
        Some(EthernetHeader$(
            source: MacAddr([1,2,3,4,5,6]),
            dest: MacAddr([1,1,1,1,1,1]),
            type_or_len: 0,
        ))
    """
    rejected_header =  """
        Some(EthernetHeader$(
            source: MacAddr([1,2,3,4,5,7]),
            dest: MacAddr([1,1,1,1,1,1]),
            type_or_len: 0,
        ))
    """
    s.i.self_.headers = accepted_header
    s.i.self_.payload = "Some(10)"
    await Timer(1, units="ns")
    s.o.headers.assert_eq(accepted_header)
    # After accepthing a header, payload is accepted until it goes None
    await FallingEdge(clk)
    s.i.self_.headers = "None"
    s.o.payload.assert_eq("Some(10)")
    await FallingEdge(clk)
    s.o.payload.assert_eq("Some(10)")
    await FallingEdge(clk)
    s.o.payload.assert_eq("Some(10)")

    s.i.self_.payload = "None"
    await FallingEdge(clk)
    s.o.payload.assert_eq("None")

    s.i.self_.headers = rejected_header
    s.i.self_.payload = "Some(10)"
    await Timer(1, units="ns")
    s.o.headers.assert_eq("None")
    await FallingEdge(clk)
    s.o.payload.assert_eq("None")
    await FallingEdge(clk)
    s.o.payload.assert_eq("None")
