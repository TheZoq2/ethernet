# top = ethernet_rx::arp_filter_th

from spade import SpadeExt
import cocotb
from cocotb.triggers import FallingEdge, Timer
from cocotb.clock import Clock

@cocotb.test()
async def packet_filter_test(dut):
    s = SpadeExt(dut)
    clk = dut.clk_i

    await cocotb.start(Clock(clk, period=10, units="ns").start())

    await FallingEdge(clk)
    s.i.rst = True;
    s.i.self_mac = "MacAddr([1,1,1,1,1,1])"
    s.i.self_.headers = "None"
    s.i.self_.payload = "None"
    await FallingEdge(clk)
    s.i.rst = False

    s.o.headers.assert_eq("None")
    s.o.payload.assert_eq("None")

    await FallingEdge(clk)

    accepted_headers =  [
        """
            Some(EthernetHeader$(
                source: MacAddr([1,2,3,4,5,6]),
                dest: MacAddr([1,1,1,1,1,1]),
                type_or_len: 0x0806,
            ))
        """,
        """
            Some(EthernetHeader$(
                source: MacAddr([1,2,3,4,5,6]),
                dest: MacAddr([0xff,0xff,0xff,0xff,0xff,0xff]),
                type_or_len: 0x0806,
            ))
        """,
    ]
    rejected_headers = [
        # Incorrect dest
        """
            Some(EthernetHeader$(
                source: MacAddr([1,2,3,4,5,6]),
                dest: MacAddr([2,2,2,2,2,2]),
                type_or_len: 0x0806,
            ))
        """,
        # Incorrect type
        """
            Some(EthernetHeader$(
                source: MacAddr([1,2,3,4,5,6]),
                dest: MacAddr([1,1,1,1,1,1]),
                type_or_len: 0x0805,
            ))
        """,
    ]
    for header in accepted_headers:
        s.i.self_.headers = header
        s.i.self_.payload = "Some(10)"
        await Timer(1, units="ns")
        s.o.headers.assert_eq(header)
        # After accepthing a header, payload is accepted until it goes None
        await FallingEdge(clk)
        s.i.self_.headers = "None"
        s.o.payload.assert_eq("Some(10)")
        await FallingEdge(clk)
        s.o.payload.assert_eq("Some(10)")
        await FallingEdge(clk)
        s.o.payload.assert_eq("Some(10)")
        s.i.self_.payload = "None"
        await FallingEdge(clk)
        s.o.payload.assert_eq("None")
        s.i.self_.payload = "None"
        await FallingEdge(clk)
        s.o.payload.assert_eq("None")

    for header in rejected_headers:
        s.i.self_.headers = header
        s.i.self_.payload = "Some(10)"
        await Timer(1, units="ns")
        s.i.self_.headers = "None"
        s.o.headers.assert_eq("None")
        await FallingEdge(clk)
        s.o.payload.assert_eq("None")
        await FallingEdge(clk)
        s.o.payload.assert_eq("None")
