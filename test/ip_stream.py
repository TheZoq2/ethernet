# top = ip::test::ip_stream_th

from cocotb.clock import Clock, Timer
from spade import List, SpadeExt
from cocotb import cocotb
from cocotb.triggers import FallingEdge

import zlib

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut) # Wrap the dut in the Spade wrapper

    clk = dut.clk_i

    async def payload_driver():
        data = list(map(lambda x: 0x10+x, range(0,100)))
        idx = 0
        while True:
            if idx >= len(data):
                s.i.payload = "None"
                break
            s.i.payload = f"Some({data[idx]})"
            if s.o.payload_ready == True:
                idx += 1
            await FallingEdge(clk)

    data: List[int] = []
    async def expect_byte(b: int, headers_ready):
        s.o.eth_byte.assert_eq(f"Some({b})")
        data.append(b)
        if headers_ready is not None:
            s.o.headers_ready.assert_eq(headers_ready)
        await FallingEdge(clk)

    async def expect_bytes(bs: List[int], headers_ready=None):
        for b in bs:
            await expect_byte(b, headers_ready)

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())

    s.i.ip_headers = "None"
    s.i.payload = "Some(0x10)"

    s.i.rst = True
    await FallingEdge(clk)
    s.i.rst = False

    s.o.headers_ready.assert_eq(True)
    s.o.payload_ready.assert_eq(False)

    await cocotb.start(payload_driver())

    payload_length = 50
    total_length = payload_length + 20;

    s.i.ip_headers = f"""
        Some(
            IpHeader$(
                // version: 4,
                // ihl goes here but is fixed to 5 since we don't support options
                // ihl: uint<4>,
                dscp: 0b1010_11,
                ecn: 0b01,
                // For now, total length cannot exceed 1480 bytes
                payload_length: {payload_length},
                identification: 0x5678,
                flags: 0b101,
                fragment_offset: 0b0_1100_11001010,
                ttl: 0x43,
                protocol: 0x56,
                // Header checksum goes here
                source: ethernet::ip::IpAddr([1,2,3,4]),
                dest: ethernet::ip::IpAddr([5,6,7,8]),
            )
        )
    """
    await FallingEdge(clk)
    s.i.ip_headers = "None"
    s.o.headers_ready.assert_eq(False)
    s.o.payload_ready.assert_eq(False)
    # We have one cycle of latency in transitioning from ip to ethernet headers. 
    await FallingEdge(clk)
    s.o.headers_ready.assert_eq(False)
    s.o.payload_ready.assert_eq(False)
    # And one cycle in ethernet header generation
    await FallingEdge(clk)
    s.o.headers_ready.assert_eq(False)
    s.o.payload_ready.assert_eq(False)
    await FallingEdge(clk)
    s.o.headers_ready.assert_eq(False)
    s.o.payload_ready.assert_eq(False)

    # Ethernet Preamble
    for _ in range(0, 7):
        s.o.eth_byte.assert_eq("Some(0x55)")
        await FallingEdge(clk)
    s.o.eth_byte.assert_eq("Some(0xD5)")
    await FallingEdge(clk)

    # Ethernet header
    await expect_bytes([11, 12, 13, 14, 15, 16], False)
    await expect_bytes([1, 2, 3, 4, 5, 6], False)
    await expect_bytes([0x08, 0x00], False)

    packet_content = [
        # First byte is fixed
        0x45,
        # dscp|ecn
        0b1010_1101,
        # Total length
        (total_length >> 8) & 0xff,
        total_length & 0xff,
        # Identification
        0x56,
        0x78,
        # Flags|FragmentOffset
        0b1010_1100,
        0b1100_1010,
        # TTL
        0x43,
        # protocol
        0x56,
        # <<Header checksum goes here>>
        # Source IP
        1,2,3,4,
        # Dest IP
        5,6,7,8
    ]

    packet_words = [(packet_content[i*2] << 8) + (packet_content[i*2+1]) for i in range(0, len(packet_content)//2)]
    print(f"{', '.join(map(lambda x: f'0x{x:02x}', packet_words))}")

    print(f"{', '.join(map(lambda x: f'0x{x:02x}', packet_content))}")
    partial_checksum = sum(packet_words)
    print(f"Partial checksum {partial_checksum}")
    checksum = ~((partial_checksum >> 16) + (partial_checksum & 0xffff))
    print(f"Checksum: {checksum}")
    content = packet_content[0:10] + [checksum >> 8 & 0xff, checksum & 0xff] + packet_content[10:18]
    print(f"{', '.join(map(lambda x: f'0x{x:02x}', content))}")
    # Start of IP header
    await expect_bytes(
        content,
        False,
    )

    # Start of ethernet payload. The payload is the total length minus the header size
    # which is 20
    print(f"Length: {total_length}")
    for i in range(0, payload_length):
        print(f"Byte {i}")
        await expect_byte(0x10+i, None)

    s.o.headers_ready.assert_eq(True)

    # No ethernet padding

    # Ethernet crc
    crc = zlib.crc32(bytes(data)) & 0xFFFFFFFF
    print(f"Crc of {list(map(lambda x: f'{x:02x}', data))} is {crc:08x} ({crc:032b})")
    for i in range(0, 4):
        byte = (crc >> (8*i)) & 0xff
        s.o.eth_byte.assert_eq(f"Some({byte})")
        s.o.headers_ready.assert_eq(True)
        await FallingEdge(clk)
    # Inter packet gap
    for i in range(0, 12):
        s.o.eth_byte.assert_eq("None")
        s.o.headers_ready.assert_eq(True)
        await FallingEdge(clk)
    s.o.headers_ready.assert_eq(True)


