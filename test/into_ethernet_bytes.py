# top = rgmii_tx::test::into_ethernet_bytes_th

from cocotb.clock import Clock, Timer
from spade import List, SpadeExt
from cocotb import cocotb
from cocotb.triggers import FallingEdge

import zlib


@cocotb.test()
async def test(dut):
    s = SpadeExt(dut) # Wrap the dut in the Spade wrapper

    async def payload_driver():
        data = list(map(lambda x: 0x10+x, range(0,10)))
        idx = 0
        while True:
            if idx >= len(data):
                s.i.data = "None"
                break
            s.i.data = f"Some({data[idx]})"
            if s.o.payload_ready == True:
                idx += 1
            await FallingEdge(clk)

    clk = dut.clk_i

    data: List[int] = []
    async def expect_byte(b: int):
        s.o.byte.assert_eq(f"Some({b})")
        data.append(b)
        await FallingEdge(clk)

    async def expect_bytes(bs: List[int]):
        for b in bs:
            await expect_byte(b)

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())

    s.i.header = "None"
    s.i.data = "Some(0x10)"

    s.i.rst = True
    [await FallingEdge(clk) for i in range(0, 10)]
    s.i.rst = False

    # Run the payload driver before after reset
    await cocotb.start(payload_driver())

    s.o.ready.assert_eq(True)

    s.i.header = """
        Some(
            ToTransmit$(
                header: EthernetHeader$(
                    source: ethernet::lib::MacAddr([0x1, 0x2, 0x3, 0x4, 0x5, 0x6]),
                    dest: ethernet::lib::MacAddr([0xde, 0xad, 0xbe, 0xef, 0x11, 0x22]),
                    type_or_len: 10,
                ),
                data_len: 10
            )
        )
    """
    await FallingEdge(clk)
    s.i.header = "None"

    s.o.ready.assert_eq(False)
    await FallingEdge(clk)
    s.o.ready.assert_eq(False)
    await FallingEdge(clk)

    for _ in range(0, 7):
        s.o.byte.assert_eq("Some(0x55)")
        await FallingEdge(clk)
    s.o.byte.assert_eq("Some(0xD5)")
    await FallingEdge(clk)

    await expect_bytes([0xde, 0xad, 0xbe, 0xef, 0x11, 0x22])
    await expect_bytes([0x1, 0x2, 0x3, 0x4, 0x5, 0x6])
    # await expect_bytes([0, 0, 0, 0])
    await expect_bytes([0, 10])

    for i in range(0, 10):
        await expect_byte(0x10+i)

    # Padding
    for i in range(0, 36):
        await expect_byte(0)

    crc = zlib.crc32(bytes(data)) & 0xFFFFFFFF
    print(f"Crc of {list(map(lambda x: f'{x:02x}', data))} is {crc:08x} ({crc:032b})")
    for i in range(0, 4):
        byte = (crc >> (8*i)) & 0xff
        s.o.byte.assert_eq(f"Some({byte})")
        await FallingEdge(clk)
    for i in range(0, 10):
        s.o.byte.assert_eq("None")
        s.o.ready.assert_eq(False)
        await FallingEdge(clk)
    s.o.ready.assert_eq(True)


@cocotb.test()
async def hello_world_udp_packet_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i

    received_data: List[int] = []

    async def expect_byte(b: int):
        s.o.byte.assert_eq(f"Some({b})")
        received_data.append(b)
        await FallingEdge(clk)

    async def expect_bytes(bs: List[int]):
        for b in bs:
            await expect_byte(b)

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())

    s.i.header = "None"
    s.i.data = "Some(0x10)"

    s.i.rst = True
    [await FallingEdge(clk) for i in range(0, 10)]
    s.i.rst = False

    s.o.ready.assert_eq(True)

    to_send = [
        0x45, 0x00, 0x00, 0x29, 0xfe, 0x00, 0x40, 0x00, 0x40, 0x11, 0xe4, 0x85, 0xac, 0x1e, 0x00, 0x00, 0xac, 0x1e, 0x00, 0x01, 0xeb, 0x80, 0x05, 0x39, 0x00, 0x15, 0x75, 0xa0, 0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x2c, 0x20, 0x57, 0x6f, 0x72, 0x6c, 0x64, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00
    ]


    async def payload_driver():
        idx = 0
        while True:
            if idx >= len(to_send):
                s.i.data = "None"
                break
            s.i.data = f"Some({to_send[idx]})"
            if s.o.payload_ready == True:
                idx += 1
            await FallingEdge(clk)

    await cocotb.start(payload_driver())

    s.i.header = f"""
        Some(
            ToTransmit$(
                header: EthernetHeader$(
                    dest: ethernet::lib::MacAddr([0x10, 0xe2, 0xd5, 0x00, 0x00, 0x00]),
                    source: ethernet::lib::MacAddr([0xa0, 0xce, 0xc8, 0xae, 0x65, 0x3c]),
                    type_or_len: 0x0800,
                ),
                data_len: {len(to_send)}
            )
        )
    """
    await FallingEdge(clk)
    s.i.header = "None"
    s.o.ready.assert_eq(False)
    await FallingEdge(clk)
    s.o.ready.assert_eq(False)
    await FallingEdge(clk)

    for _ in range(0, 7):
        s.o.byte.assert_eq("Some(0x55)")
        await FallingEdge(clk)
    s.o.byte.assert_eq("Some(0xD5)")
    await FallingEdge(clk)

    await expect_bytes([0x10, 0xe2, 0xd5, 0x00, 0x00, 0x00])
    await expect_bytes([0xa0, 0xce, 0xc8, 0xae, 0x65, 0x3c])
    # await expect_bytes([0, 0, 0, 0])
    await expect_bytes([0x08, 0x00])

    for d in to_send:
        await expect_byte(d)

    computed_crc = zlib.crc32(bytes(received_data)) & 0xFFFFFFFF
    crc = 0xcd709d4a
    print(f"Crc of {list(map(lambda x: f'{x:02x}', received_data))} is {crc:08x} computed {computed_crc:08x}")

    s.o.byte.assert_eq(f"Some(0xcd)")
    await FallingEdge(clk)
    s.o.byte.assert_eq(f"Some(0x70)")
    await FallingEdge(clk)
    s.o.byte.assert_eq(f"Some(0x9d)")
    await FallingEdge(clk)
    s.o.byte.assert_eq(f"Some(0x4a)")
    await FallingEdge(clk)

    for i in range(0, 10):
        s.o.byte.assert_eq("None")
        s.o.ready.assert_eq(False)
        await FallingEdge(clk)
    s.o.ready.assert_eq(True)
